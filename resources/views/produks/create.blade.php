@extends('default')

@section('content')

	@if($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				{{ $error }} <br>
			@endforeach
		</div>
	@endif

	{!! Form::open(['route' => 'produks.store']) !!}

		<div class="mb-3">
			{{ Form::label('nama_produk', 'Nama_produk', ['class'=>'form-label']) }}
			{{ Form::text('nama_produk', null, array('class' => 'form-control')) }}
		</div>
		<div class="mb-3">
			{{ Form::label('harga_produk', 'Harga_produk', ['class'=>'form-label']) }}
			{{ Form::text('harga_produk', null, array('class' => 'form-control')) }}
		</div>


		{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}


@stop